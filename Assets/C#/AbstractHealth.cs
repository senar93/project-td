﻿// NON CONTROLLATO A.T.

using UnityEngine;
using System.Collections;

/// <summary>
/// classe astratta che rappresenta il concetto che un oggetto possa essere vivo o morto
/// le sue implementazioni devono contenere un sistema per identificare se l'oggetto è vivo o morto, e per permettere la sua uccisione
/// </summary>
public abstract class AbstractHealth : MonoBehaviour {

	/// <summary>
	/// Restituisce gli hp massimi del oggetto
	/// </summary>
	/// <returns>hp massimi del oggetto</returns>
	public abstract float GetMaxHealth();
	/// <summary>
	/// Restituisce gli hp attuali del oggetto
	/// </summary>
	/// <returns>hp attuali del oggetto</returns>
	public abstract float GetActHealth();
	/// <summary>
	/// Restituisce se l'oggetto è vivo o morto
	/// </summary>
	/// <returns>TRUE l'oggetto è morto; FALSE l'oggetto è vivo</returns>
	public abstract bool IsDead();

	/// <summary>
	/// Infligge l'ammontare di danni passato come parametro al oggetto che possiede questo script
	/// </summary>
	/// <param name="damageValue">Ammontare di danni da infliggere</param>
	/// <returns>Danni effettivamente inflitti al oggetto</returns>
	public abstract float DealDamage(float damageValue);

}
