﻿using UnityEngine;
using System.Collections;

public abstract class AbstractCanAttack : MonoBehaviour {

	public AbstractCanTarget canTarget;
	public AbstractCanShoot canShoot;
	public AbstractDelay delay;

	void Update() {
		if (CanAttack()) {
			GameObject[] targets = canTarget.GetTarget();
			if (targets.Length > 0) {
				canShoot.Shoot(canTarget.GetTarget());
				delay.ResetDelay();
			}
		}
	}

	public bool CanAttack() {
		return delay.IsDelayExpired();
	}
}
