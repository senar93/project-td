﻿using UnityEngine;
using System.Collections;


public abstract class AbstractCanShoot : MonoBehaviour {

	public GameObject bulletPrefab;

	public abstract void Shoot(GameObject[] targets);

}
