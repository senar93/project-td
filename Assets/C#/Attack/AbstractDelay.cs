﻿using UnityEngine;
using System.Collections;


/// <summary>
/// classe astratta che rappresenta il tempo che intercorre tra un attacco e la possibilità di attaccare nuovamente
/// </summary>
public abstract class AbstractDelay : MonoBehaviour {

	/// <summary>
	/// Determines whether the delay (or cooldown) has expired whatever Its nature is.
	/// </summary>
	/// <returns><c>true</c> if this instance has the delay expired; otherwise, <c>false</c>.</returns>
	/// <limits>Timed delays should not have less than a float value of 0.1</limits>
	public abstract bool IsDelayExpired();

	/// <summary>
	/// Resets the delay variable.
	/// </summary>
	public abstract void ResetDelay();

}
