﻿using UnityEngine;
using System.Collections;


/// <summary>
/// implementazione della classe astratta AbstractDelay;
/// implementa il classico concetto di attack speed, dopo un attacco passano X secondi prima di poter attaccare nuovamente
/// </summary>
public class Standard_Delay : AbstractDelay {

	/// <summary>
	/// valore di sicurezza sotto il quale delayValue non può scendere
	/// 1/DELAY_INFERIOR_LIMIT da il valore sotto il quale gli fps non dovono mai scendere per non rischiare alcun problema (20)
	/// </summary>
	public static readonly float DELAY_INFERIOR_LIMIT = 0.05f;

	/// <summary>
	/// tempo di ricarica del attacco, 0 al momento del attacco, delayValue quando pronto a sparare
	/// </summary>
	private float timer = 0;

	// TODO: Rewrite inspector to be limited
	/// <summary>
	/// tempo tra un attacco e l'altro.
	/// un valore eccessivamente basso potrebbe causare problemi in condizioni particolari su sistemi che non arrivano a certi valori di fps
	/// </summary>
	public float delayValue = 0.5f;

	/// <summary>
	/// aggiorna timer se necessario
	/// </summary>
	void Update() {
		if (timer > delayValue)
			timer = delayValue;
		else if (timer < delayValue)
			timer += Time.deltaTime;
	}

	/// <summary>
	/// Determines whether the delay (or cooldown) has expired whatever Its nature is.
	/// </summary>
	/// <returns><c>true</c> if this instance has the delay expired; otherwise, <c>false</c>.</returns>
	/// <limits>Timed delays should not have less than a float value of 0.1</limits>
	public override bool IsDelayExpired() {
		return timer >= delayValue;
	}

	/// <summary>
	/// Resets the delay variable.
	/// </summary>
	public override void ResetDelay() {
		if (timer >= delayValue) {
			timer -= delayValue;
		}
	}


}
