﻿using UnityEngine;
using System.Collections;

public abstract class AbstractCanMove : MonoBehaviour {

	/// <summary>
	/// classe astratta che fornisce la velocità del oggetto
	/// </summary>
	public AbstractSpeed speed;
	/// <summary>
	/// classe astratta che fornisce le coordinate del elemento da raggiungere
	/// </summary>
	public AbstractPath path;

	protected bool MoveOnFixedUpdate = false;

	[SerializeField] protected int nextCoordIndex = 0;

	/// <summary>
	/// esegue il movimento del oggetto ad ogni Update/FixedUpdate
	/// </summary>
	/// <returns>TRUE se l'oggetto ha raggiunto la destinazione</returns>
	public abstract void Move();

	/// <summary>
	/// Determines whether this instance has next coordinate.
	/// </summary>
	/// <returns><c>true</c> if this instance has next coordinate; otherwise, <c>false</c>.</returns>
	public bool HasNextCoord() {
		return (path.GetReachCoord(nextCoordIndex) != null);
	}

	//esegue la funzione Move() implementata dai figli
	public void Update() {
		if (!MoveOnFixedUpdate)
			Move();
	}

	public void FixedUpdate() {
		if (MoveOnFixedUpdate)
			Move();
	}

}
