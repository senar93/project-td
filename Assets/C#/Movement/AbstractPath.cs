﻿using UnityEngine;
using System.Collections;

public abstract class AbstractPath : MonoBehaviour {

	/// <summary>
	/// restituisce le coordinate da raggiungere
	/// </summary>
	/// <param name="n">indice del nodo del percorso</param>
	/// <returns>coordinate da raggiungere</returns>
	public abstract ReachCoord GetReachCoord(int n = 0);

}
