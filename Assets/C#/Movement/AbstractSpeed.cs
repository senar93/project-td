﻿using UnityEngine;
using System.Collections;

public abstract class AbstractSpeed : MonoBehaviour {
	
	/// <summary>
	/// restituisce la velocità del oggetto
	/// </summary>
	/// <returns>velocità di spostamento del oggetto (unità/secondo)</returns>
	public abstract float GetSpeed();
	
}
