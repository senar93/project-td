﻿using UnityEngine;
using System.Collections;

public class ReachCoord {

	public static Vector2 InvalidCoord = new Vector2(999999, 999999);

	public Vector2 coords;
	public NodeTypeEnum type;


	public ReachCoord(Vector2 newCoords, NodeTypeEnum newType) {
		coords = newCoords;
		type = newType;
	}
}
