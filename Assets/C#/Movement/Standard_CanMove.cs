﻿using UnityEngine;
using System.Collections;

public class Standard_CanMove : AbstractCanMove {

	// Variabile utilizzata per mantenere una velocità media
	// costante. Se in questo frame mi posso spostare di 23
	// ma il nodo dista 16 e quindi mi sposto di 16, questa
	// variabile avrà un valore di 7 che verrà aggiunto al
	// movimento del prossimo frame.
	// NOTE: PK Considerare spalmare questo su più frame
	private float fixNextStep = 0;

	public override void Move() {

		// Ricevi il prossimo punto
		ReachCoord toReach = path.GetReachCoord(nextCoordIndex);

		if (toReach == null)
			return;

		// Vettore direzione verso il punto
		Vector2 dir = toReach.coords - (new Vector2(transform.position.x, transform.position.y));

		float distThisFrame = speed.GetSpeed() * Time.deltaTime;

		if (dir.magnitude <= distThisFrame) {
			// Snap sulla posizione precisa del punto
			transform.position = new Vector3(toReach.coords.x, toReach.coords.y);
			fixNextStep += distThisFrame - dir.magnitude;

			// Raggiunto il nodo!
			toReach = null;

			nextCoordIndex++;
		} else {
			// Spostamento in direzione del punto
			transform.Translate(new Vector3(dir.normalized.x, dir.normalized.y, 0) * (distThisFrame + fixNextStep), Space.World);
			// TODO: PK Ottimizzare questo ciclo
			fixNextStep = 0;

			// Rotazione smoothed verso il punto
			Quaternion targetRotation = Quaternion.LookRotation(new Vector3(dir.x, dir.y, 0));
			Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 5);
		}

	}
}
