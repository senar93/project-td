﻿// NON CONTROLLATO A.T.

using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Implementazione di AbstractHealth
/// implementa il concetto di hp classico, un valore che viene decrementato dei danni subiti fino a raggiungere lo 0
/// </summary>
public class Standard_Health : AbstractHealth {

	/// <summary>
	/// limite inferiore per maxHealth e actHealth
	/// </summary>
	public static readonly float HEALTH_INFERIOR_LIMIT = 0f;

	/// <summary>
	/// hp attuali del oggetto
	/// </summary>
	public float actHealth;
	/// <summary>
	/// hp massimi del oggetto (solitamente quelli assegnati al momento della creazione)
	/// </summary>
	public float maxHealth;


	/// <summary>
	/// costruttore generico della classe Standard_Health
	/// </summary>
	public Standard_Health() {; }
	/// <summary>
	/// costruttore della classe Standard_Health, imposta automaticamente i parametri del oggetto con i valori appropiati
	/// </summary>
	/// <param name="maxHealth">hp del unità massimi e attuali dell oggetto al momento della creazione</param>
	public Standard_Health(float maxHealth) {
		if(maxHealth > HEALTH_INFERIOR_LIMIT) {
			this.maxHealth = maxHealth;
			this.actHealth = maxHealth;
		} else {
			this.maxHealth = HEALTH_INFERIOR_LIMIT;
			this.actHealth = HEALTH_INFERIOR_LIMIT;
		}
	}

	


	/// <summary>
	/// Restituisce gli hp massimi del oggetto
	/// </summary>
	/// <returns>hp massimi del oggetto</returns>
	public override float GetMaxHealth() {
		return maxHealth;
	}

	/// <summary>
	/// Restituisce gli hp attuali del oggetto
	/// </summary>
	/// <returns>hp attuali del oggetto</returns>
	public override float GetActHealth() {
		return actHealth;
	}

	/// <summary>
	/// Restituisce se l'oggetto è vivo o morto
	/// </summary>
	/// <returns>TRUE l'oggetto è morto; FALSE l'oggetto è vivo</returns>
	public override bool IsDead() {
		return actHealth <= HEALTH_INFERIOR_LIMIT;
	}



	/// <summary>
	/// Infligge l'ammontare di danni passato come parametro al oggetto che possiede questo script
	/// </summary>
	/// <param name="damageValue">Ammontare di danni da infliggere</param>
	/// <returns>Danni effettivamente inflitti al oggetto</returns>
	public override float DealDamage(float damageValue) {
		actHealth -= damageValue;
		if (actHealth >= HEALTH_INFERIOR_LIMIT)
			return damageValue;
		else
			return damageValue + actHealth;
	}


}
