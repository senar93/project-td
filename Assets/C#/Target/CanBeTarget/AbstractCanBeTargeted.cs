﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AbstractCanBeTargeted : MonoBehaviour {

	/// <summary>
	/// restituisce  il tipo di target posseduto dall oggetto
	/// </summary>
	/// <returns>tipo di target posseduto dall oggetto</returns>
	public abstract TargetTypeEnum[] GetTargetType();

}
