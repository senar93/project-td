﻿using UnityEngine;
using System.Collections;

public abstract class AbstractCanTarget : MonoBehaviour {

	
	public AbstractRangeShape rangeShape;

	/// <summary>
	/// restituisce un array contenente il puntatore agli oggetti selezionati
	/// </summary>
	/// <param name="targetMode">modalità di selezione del bersaglio</param>
	/// <param name="n">numero di bersagli da selezionare</param>
	/// <returns>array di puntatore a GameObject</returns>
	public abstract GameObject[] GetTarget(TargetModeEnum targetMode = TargetModeEnum.Standard, int n = 1);

	/// <summary>
	/// Checks the range.
	/// </summary>
	/// <returns><c>true</c>, if range was checked, <c>false</c> otherwise.</returns>
	/// <param name="obj">Object.</param>
	public bool CheckRange(GameObject obj) {
		return rangeShape.CheckRange(obj);
	}

}
