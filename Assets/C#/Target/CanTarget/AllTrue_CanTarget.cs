﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AllTrue_CanTarget : AbstractCanTarget {

	public List<TargetTypeEnum> typeList;

	public TEST_Manager manager;
	public TargetableStructure targets;

	public override GameObject[] GetTarget(TargetModeEnum targetMode = TargetModeEnum.Standard, int n = 1) {

		List<GameObject> result = new List<GameObject>();

		// Ciclo su ciascun typeList e chiedo a target qual è la lista con meno
		// risultati
		int lowerObjCount = int.MaxValue;
		int lowerObjCountIndex = 0;

		for (int i = 0; i < typeList.Count; i++) {
			if (lowerObjCount > targets[(int)typeList[i]].Count) {
				lowerObjCount = targets[(int)typeList[i]].Count;
				lowerObjCountIndex = (int)typeList[i];
			}
		}


		// Ciclo su ciascun oggetto
		for (int i = 0; i < lowerObjCount; i++) {
			
			TargetTypeEnum[] objTypeList = targets[lowerObjCountIndex][i].GetComponent<AbstractCanBeTargeted>().GetTargetType();

			int typesFoundCounter = 0;

			// Ciclo su ciascun tipo in typeList
			for (int j = 0; j < typeList.Count && typesFoundCounter == j; j++) {

				// Ciclo su ciascun tipo in objTypeList
				for (int m = 0; m < objTypeList.Length; m++) {
					if (objTypeList[m] == typeList[j])
						typesFoundCounter++;
				}
			}

			if (typesFoundCounter == typeList.Count) {
				if (CheckRange(targets[lowerObjCountIndex][i]))
					result.Add(targets[lowerObjCountIndex][i]);
			}

		}

		return result.ToArray();
	}
}
