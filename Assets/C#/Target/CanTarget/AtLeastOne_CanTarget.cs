﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AtLeastOne_CanTarget : AbstractCanTarget {

	public List<TargetTypeEnum> typeList;

	public AbstractManager manager;
	public TargetableStructure targets;

	void Start() {
		manager = FindObjectOfType<AbstractManager>();
		targets = manager.targets;

		Debug.Log(manager.name);

		GameObject[] objs = GetTarget(TargetModeEnum.Standard, 3);

		foreach (var obj in objs) {
			Debug.Log("I'm hitting: " + obj.name);
		}
	}

	public override GameObject[] GetTarget(TargetModeEnum targetMode = TargetModeEnum.FirstEnter, int n = 1) {

		List<GameObject> result = new List<GameObject>();

		for (int i = 0; i < typeList.Count; i++) {
			for (int j = 0; j < targets[(int)typeList[i]].Count; j++) {
				GameObject tg = targets[(int)typeList[i]][j];

				if (CheckRange(tg) && !result.Contains(tg))
					result.Add(tg);
			}
		}

		return result.ToArray();
	}

}
