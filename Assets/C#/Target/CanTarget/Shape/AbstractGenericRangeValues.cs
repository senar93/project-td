﻿using UnityEngine;
using System.Collections;

/// <summary>
/// classe astratta che implementerà i metodi e i parametri comuni a tutte le classi astratte
///		che rappresentano i parametri per identificare l'area di target delle varie figure
/// </summary>
public abstract class AbstractGenericRangeValues : MonoBehaviour {
	
}
