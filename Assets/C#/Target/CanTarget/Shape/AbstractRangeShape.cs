﻿using UnityEngine;
using System.Collections;

/// <summary>
/// classe astratta che implementa la forma del "area di targettabile" entro cui un oggetto può targettarne un altro
/// </summary>
public abstract class AbstractRangeShape : MonoBehaviour {


	/// <summary>
	/// Gets the range shape.
	/// </summary>
	/// <returns>The range shape.</returns>
	public abstract bool CheckRange(GameObject obj);
}
