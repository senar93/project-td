﻿using UnityEngine;
using System.Collections;

/// <summary>
/// classe astratta che implementa il concetto di distanza entro il quale l'oggetto può targettare altri oggetti;
/// indipendentemente dal fatto che la forma del "area di targettabile" sia circolare, conica o altro
/// </summary>
public abstract class AbstractCircleRangeValue : AbstractGenericRangeValues {

	/// <summary>
	/// Gets the range.
	/// </summary>
	/// <returns>The range.</returns>
	public abstract float GetRange();

}
