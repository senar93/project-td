﻿using UnityEngine;
using System.Collections;

/// <summary>
/// implementazione di AbstractRangeShape, implementa il cerchio come possibile forma del "area di targettabile"
/// </summary>
public class Circular_RangeShape : AbstractRangeShape {

	/// <summary>
	/// oggetto che calcola il raggio del cerchio
	/// </summary>
	public AbstractCircleRangeValue range;

	/// <summary>
	/// Gets the range shape.
	/// </summary>
	/// <returns>The range shape.</returns>
	public override bool CheckRange(GameObject obj) {
		return Vector3.Distance(transform.position, obj.transform.position) <= range.GetRange();
	}

}
