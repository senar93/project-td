﻿using UnityEngine;
using System.Collections;

/// <summary>
/// implementazione di AbstractRangeValue, implementa un valore fisso di raggio (ampliezza del cerchio, lunghezza linea ecc)
/// </summary>
public class Linear_CircularRangeValue : AbstractCircleRangeValue {

	public float range = 3.5f;

	public override float GetRange() {
		return range;
	}

	// TODO: Cancellare, serve solo per visualizzazione durante i test
	void OnDrawGizmos() {
		Gizmos.color = Color.black;
		Gizmos.DrawWireSphere(transform.position, range);
	}
}
