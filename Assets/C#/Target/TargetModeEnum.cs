﻿/// <summary>
/// enum che rappresenta i vari modi di selezionare il/i bersagli
/// </summary>
public enum TargetModeEnum {

	/// <summary>
	/// seleziona il primo nemico entrato nell area
	/// </summary>
	FirstEnter = 1,
	/// <summary>
	/// seleziona l'ultimo nemico entrato nell area
	/// </summary>
	LastEnter = 2,
	/// <summary>
	/// seleziona il nemico con la vita attuale più alta
	/// </summary>
	HighActLife = 3,
	/// <summary>
	/// seleziona il nemico con la vita attuale più bassa
	/// </summary>
	LowActLife = 4,
	/// <summary>
	/// seleziona il nemico con la vita totale più alta
	/// </summary>
	HighMaxLife = 5,
	/// <summary>
	/// seleziona il nemico con la vita totale più bassa
	/// </summary>
	LowMaxLife = 6,
	/// <summary>
	/// seleziona il nemico più veloce
	/// </summary>
	HighSpeed = 7,
	/// <summary>
	/// seleziona il nemico più lento
	/// </summary>
	LowSpeed = 8,

	/// <summary>
	/// seleziona la modalità di target Standard (FirstEnter)
	/// </summary>
	Standard = 1
}
