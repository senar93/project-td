﻿/// <summary>
/// enum che rappresenta i vari tipi di oggetti che possono essere selezionati come target
/// </summary>
public enum TargetTypeEnum {

	/// <summary>
	/// indica che l'oggetto è una torre
	/// </summary>
	tower = 1,
	/// <summary>
	/// indica che l'oggetto è un mostro che cammina sul terreno
	/// </summary>
	groundMonster = 2,

	/// <summary>
	/// indica che l'oggetto non può essere targettato
	/// </summary>
	unselectable = 0
}