﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// classe che implementa il contenitore per tutti gli oggetti del gioco;
/// qualunque oggetto che può essere targettato si trova in questa struttura
/// </summary>
public class TargetableStructure {

	public List<List<GameObject>> targetList;

	/// <summary>
	/// numero di oggetti contenuti nella struttura
	/// </summary>
	public int Count {
		get { return targetList.Count; }
	}

	public List<GameObject> this [int a] {
		get { return GetList(a); }
	}

	public GameObject this [int a, int b] {
		get { return GetObject(a, b); }
	}

	/// <summary>
	/// costruttore della classe TargetableStructure;
	/// popola automaticamente la lista con tante liste quanti sono i tipi possibili definiti in TargetTypeEnum
	/// </summary>
	public TargetableStructure() {

		targetList = new List<List<GameObject>>();

		// NOTE: PK Caso fosse utilizzeremo più volte la classe Enum
		// si consideri l'ampliamento della stessa con funzioni o
		// parametri come Max(), Average() ecc.
		int indexCount = Enum.GetValues(typeof(TargetTypeEnum)).Length;

		for (int i = 0; i < indexCount; i++) {
			targetList.Add(new List<GameObject>());
		}

	}

	/// <summary>
	/// restituisce la lista contenente tutti gli oggetti con tag (int)TargetTypeEnum
	/// </summary>
	/// <param name="a">(int)TargetTypeEnum.TipoSelezionato</param>
	/// <returns>lista oggetti targetabili del tipo selezionato</returns>
	public List<GameObject> GetList(int a) {
		if (a >= 0 && a < targetList.Count)
			return targetList[a];
		return null;
	}

	/// <summary>
	/// estrae un oggetto specifico dalla lista
	/// </summary>
	/// <param name="a">(int)TargetTypeEnum.TipoSelezionato</param>
	/// <param name="b">indice del elemento da estrarre</param>
	/// <returns>oggetto selezionato</returns>
	public GameObject GetObject(int a, int b) {
		if (a >= 0 && a < targetList.Count && b >= 0 && b < targetList[a].Count)
			return targetList[a][b];
		return null;
	}

	/// <summary>
	/// inserisce un oggetto selezionabile nella posizione appropriata
	/// </summary>
	/// <param name="obj">oggetto da aggiungere, deve contenere un implementazione di AbstractCanBeTargeted</param>
	public void InsertElement(GameObject obj) {
		if (obj == null || obj.GetComponent<AbstractCanBeTargeted>() == null)
			return;
		
		TargetTypeEnum[] types = obj.GetComponent<AbstractCanBeTargeted>().GetTargetType();

		for (int i = 0; i < types.Length; i++) {
			targetList[(int)types[i]].Add(obj);
		}
	}

	/// <summary>
	/// rimuove l'oggetto passato come parametro dalla struttura se presente
	/// </summary>
	/// <param name="obj">oggetto da rimuovere</param>
	public void RemoveElement(GameObject obj) {
		if (obj == null)
			return;
		
		for (int i = 0; i < targetList.Count; i++) {
			targetList[i].Remove(obj);
		}
	}

}