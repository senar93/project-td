﻿using UnityEngine;
using System.Collections;

public class TEST_CanBeTargeted : AbstractCanBeTargeted {

	public TargetTypeEnum[] types;

	public override TargetTypeEnum[] GetTargetType() {
		return types;
	}
}
