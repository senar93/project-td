﻿using UnityEngine;
using System.Collections;

public class TEST_Path : AbstractPath {

	public GameObject[] objectToReach;

	public override ReachCoord GetReachCoord(int n = 0) {
		if (objectToReach != null)
			return new ReachCoord(new Vector2(objectToReach[n].transform.position.x, objectToReach[n].transform.position.y),
								  NodeTypeEnum.walk);

		return null;
	}
}
