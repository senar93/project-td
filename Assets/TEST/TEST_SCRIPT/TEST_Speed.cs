﻿using UnityEngine;
using System.Collections;

public class TEST_Speed : AbstractSpeed {

	public float speed = 0;

	public override float GetSpeed() {
		return speed;
	}
}
