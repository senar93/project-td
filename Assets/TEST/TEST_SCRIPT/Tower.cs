﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour {

	public float TowerRange;

	void OnDrawGizmos() {
		Gizmos.color = Color.black;
		Gizmos.DrawWireSphere(transform.position, TowerRange);
	}
}
