﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TEST_Manager : AbstractManager {

	public List<GameObject> enemies;

	void Awake() {
		targets = new TargetableStructure();

		foreach (var enemy in enemies) {
			targets.InsertElement(enemy);
		}

		for (int i = 0; i < targets.targetList.Count; i++) {
			Debug.Log(targets[i].Count);
		}
	}
}
