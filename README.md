## **TORRI** ##
* **torre base**: torre discretamente efficace in ogni situazione (ma non eccelle in nessuna), ha un buon rapporto efficacia/prezzo il che la rende particolarmente efficente per gestire le prime wave, con il proseguire della partita torri più specifiche risulteranno più efficaci, se scelte con cura; eventuali level up potenziano le statistiche della torre

    1. **slow**: questa evoluzione aggiunge l'effetto slow agli attacchi della torre, i nemici colpiti dal attacco saranno rallentati per un certo tot di tempo, l'evoluzione non fornisce un significativo aumento alle statistiche della torre, eventuali level up potenzieranno in particolare lo slow

    2. **gold**: questa evoluzione potenzia discretamente le statistiche della torre, inoltre i nemici uccisi dalla torre forniscono un ammontare di gold più elevato, eventuali level up potenzieranno le statistiche della torre e i gold guadagnati



* **laser continuo**: spara un raggio laser verso un nemico, i danni di un singolo colpo del laser sono bassi, ma vengono riapplicati molto rapidamente quindi il dps della torre è elevato; quando deve cambiare target la torre necessita di un breve lasso di tempo prima di poter riprendere a far fuoco, queste torri risultano particolarmente efficaci con wave composte da pochi nemici lenti, minore è il numero dei nemici (e più lentamente escono dal raggio della torre) meno frequentemente dovrà cambiare target massimizzando il dps, l'efficacia di questa torre diminuisce sempre di più avvicinandosi al estremo opposto, una wave con moltissimi nemici molto veloci

    1. **crescita**: questa evoluzione massimizza il dps si un singolo bersaglio, il raggio cresce di potenza ogni volta che fa danni allo stesso target aumentando i danni che infligge, cambiare target resetta il bonus

    2. **distribuzione**: questa evoluzione non aumenta il dps sul singolo target della torre, rendendola di fatto uguale alla versione base, il raggio però dopo aver colpito il primo target rimbalzerà su un certo numero di altri bersagli infliggendo gli stessi danni inflitti al bersaglio principale